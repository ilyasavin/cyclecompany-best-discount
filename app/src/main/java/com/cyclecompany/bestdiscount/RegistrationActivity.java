package com.cyclecompany.bestdiscount;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.register.RegistrationData;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;
import com.cyclecompany.bestdiscount.util.PrefUtil;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import java.io.UnsupportedEncodingException;

import butterknife.Bind;

import static android.view.View.GONE;

public class RegistrationActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.input_name)
    EditText inputNameEdit;
    @Bind(R.id.input_password)
    EditText inputPasswordEdit;
    @Bind(R.id.input_password_confirm)
    EditText inputPasswordEditConfirm;
    @Bind(R.id.input_phone)
    EditText inputPhoneEdit;
    @Bind(R.id.registrationLayout)
    ViewGroup registrationLayout;
    @Bind(R.id.progressBar)
    ProgressBar mProgressBar;

    @Bind(R.id.btn_sign_in)
    Button signUpBtn;

    @Bind(R.id.input_layout_password)
    TextInputLayout inputPasswordLayout;
    @Bind(R.id.input_layout_password_confirm)
    TextInputLayout inputPasswordConfirmLayout;
    @Bind(R.id.input_layout_name)
    TextInputLayout inputNameLayout;
    @Bind(R.id.input_layout_phone)
    TextInputLayout inputPhoneLayout;


    String userRegId;

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mToolbar.setTitle("Регистрация");

        if (getIntent() != null)
            userRegId = getIntent().getStringExtra("user_id");

        mToolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        inputNameEdit.setInputType(InputType.TYPE_CLASS_TEXT);

        inputPasswordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        inputPasswordEdit.addTextChangedListener(new MyTextWatcher(inputPasswordEdit));
        inputNameEdit.addTextChangedListener(new MyTextWatcher(inputNameEdit));
        inputPhoneEdit.addTextChangedListener(new MyTextWatcher(inputPhoneEdit));


        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    submitForm();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void signUpRest() throws UnsupportedEncodingException {

        registrationLayout.setVisibility(GONE);
        mProgressBar.setVisibility(View.VISIBLE);


        DataService.init().signUpUser(RegistrationActivity.this, new DataService.onRequestSignUpResult() {
                    @Override
                    public void onRequestSignUpResult(RegistrationData responseMessage, String cookie) {
                        mProgressBar.setVisibility(GONE);
                        registrationLayout.setVisibility(View.INVISIBLE);
                        if (responseMessage.getMessage().getCode() == 10) {
                            if (responseMessage.getData().size() > 0) {
                                if (responseMessage.getMessage().getValue() != null)
                                    RealmUtils.addUserSettings(realm,
                                            responseMessage.getData().get(0));
                                PrefUtil.setIsAuthorized(RegistrationActivity.this);
                                finish();
                                ActivityNavigator.startAnketa(RegistrationActivity.this);
                            }
                        } else
                            Toast.makeText(RegistrationActivity.this, responseMessage.getMessage().getValue(), Toast.LENGTH_SHORT).show();
                    }
                }
                , inputNameEdit.getText().toString(),
                inputPhoneEdit.getText().toString(),
                inputPasswordEdit.getText().toString(),
                inputPasswordEditConfirm.getText().toString()
                , ""
        );

    }

    private void submitForm() throws UnsupportedEncodingException {

        signUpRest();

    }

    private boolean validatePassword() {
        if (inputPasswordEdit.getText().toString().trim().isEmpty()) {
            inputPasswordLayout.setError("Ошибка пароля");
            requestFocus(inputPasswordEdit);
            return false;
        } else {
            inputPasswordLayout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePhone() {
        if (inputPhoneEdit.getText().toString().trim().isEmpty()) {
            inputPhoneLayout.setError("Неправильный номер");
            requestFocus(inputPhoneEdit);
            return false;
        } else {
            inputPhoneLayout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean checkPasswords() {
        if (!inputPasswordEdit.getText().toString().equals(inputPasswordEditConfirm.getText().toString())) {
            inputPasswordConfirmLayout.setError("Пароли не совпадают");
            requestFocus(inputPasswordEditConfirm);
            return false;
        } else {
            inputPasswordConfirmLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateName() {
        if (inputNameEdit.getText().toString().trim().isEmpty()) {
            inputNameLayout.setError("Ошибка пароля");
            requestFocus(inputNameEdit);
            return false;
        } else {
            inputNameLayout.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.input_password:
                    validatePassword();
                    break;
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_phone:
                    validatePhone();
                    break;
            }
        }
    }

}
