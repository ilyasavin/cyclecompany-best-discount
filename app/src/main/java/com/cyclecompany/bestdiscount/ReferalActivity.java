package com.cyclecompany.bestdiscount;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.cyclecompany.bestdiscount.adapters.ReferalsRVAdapter;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.referals.ReferalModel;
import com.cyclecompany.bestdiscount.model.referals.ReferalsData;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import butterknife.Bind;

public class ReferalActivity extends BaseActivity {

    @Bind(R.id.postImage)
    ImageView mPostImage;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.rv)
    RecyclerView mRecyclerView;

    ReferalModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referal);

        userModel = getIntent().getParcelableExtra("Referal");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            mPostImage.setTransitionName("ReferalTrans");
        }

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(layoutManager);


        mToolbar.setNavigationIcon(R.drawable.ic_arrow_left_black_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.showOverflowMenu();

        mToolbar.setTitle(userModel.getUsername());


        mPostImage.setImageResource(R.color.accent);

        getReferalsToLayout();


    }

    private void getReferalsToLayout() {

        DataService.init().getUserReferals
                (new DataService.onRequestReferalsResult() {
                     @Override
                     public void onRequestReferalsResult(ReferalsData referalsData) {

                         ReferalsRVAdapter refAdapter = new ReferalsRVAdapter(ReferalActivity.this, referalsData.getData(), false);
                         mRecyclerView.setAdapter(refAdapter);
                     }
                 }, userModel.getId(),
                        RealmUtils.getUserSettings(BaseActivity.realm).getUserModelRealm().getToken()
                );


    }

}
