package com.cyclecompany.bestdiscount.components;

/**
 * A constants are used in application.
 */
public class Constants {

    public static final String IMAGE_TRANSITION = "image_anim";
    public static final String POST_EXTRA = "Post";
    public static final int LOADING_TIME = 1000;
    public static final int LOADING_INTERVAL = 200;
    public static final int POST_TEXT_LENGTH = 100;
    public static final int ANIM_DURATION_TOOLBAR = 300;
    public static final String SELECT_CITY_DIALOG = "selectDialog";

}
