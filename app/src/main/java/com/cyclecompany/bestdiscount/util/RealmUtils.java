package com.cyclecompany.bestdiscount.util;

import com.cyclecompany.bestdiscount.model.realm.SettingsObject;
import com.cyclecompany.bestdiscount.model.register.UserModel;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Contains methods for realm database
 */
public class RealmUtils {

    public static boolean checkIfExists(Realm realm, int id) {

        RealmQuery<SettingsObject> query = realm.where(SettingsObject.class)
                .equalTo("id", id);

        return query.count() == 0 ? false : true;
    }

    public static boolean addUserSettings(Realm realm, UserModel userModel) {

            realm.beginTransaction();
            SettingsObject postRealmModel = new SettingsObject(

                    userModel

            );

            realm.copyToRealm(postRealmModel);
            realm.commitTransaction();
            return true;
        }



    public static SettingsObject getUserSettings(Realm realm){
        RealmResults<SettingsObject> realmQuery = realm.where(SettingsObject.class)
                .findAll();

        return realmQuery.get(0);
    }
}