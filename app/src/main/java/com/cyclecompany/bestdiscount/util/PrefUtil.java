package com.cyclecompany.bestdiscount.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PrefUtil {

    public static void setIsAuthorized(Context activity) {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(activity).edit();
        prefs.putInt("isAuthorized", 1);
        prefs.apply();
    }
}
