package com.cyclecompany.bestdiscount;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.cyclecompany.bestdiscount.components.Constants;
import com.cyclecompany.bestdiscount.fragments.AuthorizationFragment;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;


public class FirstStartActivity extends BaseActivity implements View.OnClickListener {

    private Button btnRegistration;
    private Button btnScan;
    private AuthorizationFragment authorizationFragment;
    private LinearLayout buttons;
    private ImageView icon;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_first_start);

        Intent intent = new Intent(this, IntroActivity.class);
        startActivity(intent);

        buttons = (LinearLayout) findViewById(R.id.buttonsLayout);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        btnScan = (Button)findViewById(R.id.btnScan);
        icon = (ImageView) findViewById(R.id.icon);
        buttons.animate();

        btnRegistration = (Button) findViewById(R.id.btnRegistration);

        btnRegistration.setOnClickListener(this);
        btnScan.setOnClickListener(this);

        ResizeIcon();

        int actionbarSize = dpToPx(this,56);

        btnRegistration.setTranslationY(actionbarSize);

        btnRegistration.animate()
                .translationY(0)
                .setDuration(Constants.ANIM_DURATION_TOOLBAR)
                .setStartDelay(100);

        ResizeIcon();
        authorizationFragment = new AuthorizationFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.down_to_top, R.anim.up_from_bottom);
        transaction.replace(R.id.login_fragment, authorizationFragment);
        transaction.commit();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegistration:
                //setIsFirstStartToFalse();
                ActivityNavigator.startRegistrationActivity(this, null);
                finish();
                break;
            case R.id.btnScan:
                ActivityNavigator.startScannerActivity(this);
                break;
        }
    }

    private void setIsFirstStartToFalse() {
        SharedPreferences settings;
        settings = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        settings.edit().putBoolean("isFirstStart", false);
        settings.edit().putBoolean("isFirstSettings", true);
        settings.edit().putBoolean("isAuthorized", true);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("isFirstStart", false);
        editor.putBoolean("isFirstSettings", true);
        editor.commit();
    }


    public void ResizeIcon() {
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(icon,
                PropertyValuesHolder.ofFloat("scaleX", 0.8f),
                PropertyValuesHolder.ofFloat("scaleY", 0.8f));
        scaleDown.setDuration(500);
        scaleDown.start();
    }

    public void SlideToDown() {

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 5.2f);

        slide.setDuration(500);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        buttons.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                buttons.setVisibility(View.GONE);

            }

        });
    }

    private static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics =  context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}