package com.cyclecompany.bestdiscount;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.anketa.AnketaPostData;
import com.cyclecompany.bestdiscount.model.photo.PhotoData;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import butterknife.Bind;

public class AnketaActivity extends BaseActivity {

    @Bind(R.id.btn_next)
    Button btnNext;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.cars)
    CheckBox cars;

    @Bind(R.id.it)
    CheckBox it;

    @Bind(R.id.art)
    CheckBox art;

    @Bind(R.id.health)
    CheckBox health;

    @Bind(R.id.neededucation)
    CheckBox needEducation;

    @Bind(R.id.clothes)
    CheckBox clothes;

    @Bind(R.id.food)
    CheckBox food;

    @Bind(R.id.sport)
    CheckBox sport;

    @Bind(R.id.home)
    CheckBox home;

    @Bind(R.id.video)
    CheckBox video;

    @Bind(R.id.sushi)
    CheckBox sushi;

    @Bind(R.id.games)
    CheckBox games;

    @Bind(R.id.pizza)
    CheckBox pizza;

    @Bind(R.id.sauna)
    CheckBox sauna;

    @Bind(R.id.nightclub)
    CheckBox nightclub;

    @Bind(R.id.drink)
    CheckBox drink;

    @Bind(R.id.family)
    Spinner family;
    String familyString;

    @Bind(R.id.car)
    EditText car;
    String carString;

    @Bind(R.id.education)
    Spinner education;
    String educationString;

    @Bind(R.id.myChoose)
    EditText myChoose;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anketa);

        mToolbar.setTitle("Анкета");

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String myChooseText = myChoose.getText().toString();

                AnketaPostData data = new AnketaPostData(
                        Integer.parseInt(RealmUtils.getUserSettings(realm).getUserModelRealm().getId()),
                        String.valueOf(cars.isChecked() ? 1 : 0),
                        String.valueOf(it.isChecked() ? 1 : 0),
                        String.valueOf(art.isChecked() ? 1 : 0),
                        String.valueOf(health.isChecked() ? 1 : 0),
                        String.valueOf(needEducation.isChecked() ? 1 : 0),
                        String.valueOf(clothes.isChecked() ? 1 : 0),
                        String.valueOf(food.isChecked() ? 1 : 0),
                        String.valueOf(sport.isChecked() ? 1 : 0),
                        String.valueOf(home.isChecked() ? 1 : 0),
                        String.valueOf(video.isChecked() ? 1 : 0),
                        String.valueOf(sushi.isChecked() ? 1 : 0),
                        String.valueOf(games.isChecked() ? 1 : 0),
                        String.valueOf(pizza.isChecked() ? 1 : 0),
                        String.valueOf(sauna.isChecked() ? 1 : 0),
                        String.valueOf(nightclub.isChecked() ? 1 : 0),
                        String.valueOf(drink.isChecked() ? 1 : 0),
                        myChooseText,
                        String.valueOf(familyString),
                        String.valueOf(car.getText().toString()),
                        String.valueOf(educationString)
                        );

                DataService.init().sendAnketa(new DataService.onRequestAnketaResult() {
                    @Override
                    public void onRequestAnketaResult(PhotoData photoData) {
                        ActivityNavigator.startMainActivity(AnketaActivity.this);
                        finish();
                    }
                }, Integer.parseInt(RealmUtils.getUserSettings(realm).getUserModelRealm().getId()),
                        RealmUtils.getUserSettings(realm).getToken(), data );
            }
        });

        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.family, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        family.setAdapter(adapter);

        ArrayAdapter<?> adapter2 =
                ArrayAdapter.createFromResource(this, R.array.education, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        education.setAdapter(adapter2);

        family.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                String[] choose = getResources().getStringArray(R.array.family);
                //Toast toast = Toast.makeText(getApplicationContext(),
                //        "Ваш выбор: " + choose[selectedItemPosition], Toast.LENGTH_SHORT);
                //toast.show();

                familyString = choose[selectedItemPosition];

            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        education.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                String[] choose = getResources().getStringArray(R.array.education);
                //Toast toast = Toast.makeText(getApplicationContext(),
                //        "Ваш выбор: " + choose[selectedItemPosition], Toast.LENGTH_SHORT);
                //toast.show();

                educationString = choose[selectedItemPosition];
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }
}
