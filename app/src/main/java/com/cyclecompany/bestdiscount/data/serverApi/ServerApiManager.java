package com.cyclecompany.bestdiscount.data.serverApi;

import com.cyclecompany.bestdiscount.model.FriendInviteCode;
import com.cyclecompany.bestdiscount.model.anketa.AnketaPostData;
import com.cyclecompany.bestdiscount.model.categories.CategoriesData;
import com.cyclecompany.bestdiscount.model.login.LoginData;
import com.cyclecompany.bestdiscount.model.login.RequestSignInModel;
import com.cyclecompany.bestdiscount.model.news.NewsData;
import com.cyclecompany.bestdiscount.model.photo.PhotoData;
import com.cyclecompany.bestdiscount.model.pincode.PinCodeData;
import com.cyclecompany.bestdiscount.model.referals.ReferalsData;
import com.cyclecompany.bestdiscount.model.register.RegistrationData;
import com.cyclecompany.bestdiscount.model.register.RequestSignUpModel;
import com.cyclecompany.bestdiscount.model.shop_history.ShopHistoryData;
import com.cyclecompany.bestdiscount.model.shops.AllShopsData;
import com.cyclecompany.bestdiscount.model.specialnews.SpecialNewsData;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * A manager that allows get data from network using Retrofit
 */
public class ServerApiManager {

    private static ApiService apiService;
    private static final String URL = "http://thebest-d.com/new/api";
    public interface ApiService {

        @GET("/specialNews/getSpecialNews")
        void getSpecialNews(@Query("token") String token, Callback<SpecialNewsData> response);

        @GET("/Shop/getCategories")
        void getCategories(@Query("token") String token, Callback<CategoriesData> response);

        @GET("/News/getNews")
        void getNews(@Query("token") String token, Callback<NewsData> response);

        @GET("/user/getPinCode")
        void getPincode(@Query("userid") String userid,@Query("token") String token, Callback<PinCodeData> response);

        @GET("/login/getAccountById")
        void getUserInfo(@Query("userid") String userid,@Query("token") String token, Callback<LoginData> response);

        @GET("/Shop/getAllShops")
        void getAllShops(@Query("token") String token, Callback<AllShopsData> response);

        @GET("/Shop/getPurchaseHistory")
        void getShopHistory(@Query("userid") String userid,@Query("token") String token, Callback<ShopHistoryData> response);

        @GET("/referals/getReferalFriends")
        void getReferals(@Query("userid") String userid,@Query("token") String token, Callback<ReferalsData> response);


        @POST("/login/register")
        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        void signUpUser(
                @Body RequestSignUpModel requestSignUpModel,
                Callback<RegistrationData> response);

        @POST("/login/login")
        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        void signInUser(
                @Body RequestSignInModel requestSignInModel,
                Callback<LoginData> response);

        @POST("/login/login")
        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        void signInUserWithToken(
                @Body RequestSignInModel requestSignInModel,
                Callback<LoginData> response);

        //Invite referal

        @GET("/index.php")
        void getReferalCodeForFriend (@Query("option") String option, @Query("task") String task,
                        @Query("token") String token,
                        Callback<FriendInviteCode> responseCode);

        @Multipart
        @POST("/user/uploadPhoto")
        void uploadFile(@Part("photo") TypedFile userPhoto, Callback<PhotoData> callback);

        @POST("/user/sendUserAnketa")
        void sendUserAnketa(@Query("token") String token, @Body AnketaPostData anketaPostData,
                            Callback<PhotoData> photoDataCallback);



    }

    public static ApiService getApiService() {

            Executor executor = Executors.newCachedThreadPool();
            RestAdapter restAdapter = new RestAdapter.Builder().
                    setEndpoint(URL).build();
            apiService = restAdapter.create(ApiService.class);

        return apiService;


    }

}
