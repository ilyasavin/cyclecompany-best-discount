package com.cyclecompany.bestdiscount.data;

import android.app.Activity;
import android.content.Context;

import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.data.serverApi.ServerApiManager;
import com.cyclecompany.bestdiscount.model.FriendInviteCode;
import com.cyclecompany.bestdiscount.model.anketa.AnketaPostData;
import com.cyclecompany.bestdiscount.model.categories.CategoriesData;
import com.cyclecompany.bestdiscount.model.login.LoginData;
import com.cyclecompany.bestdiscount.model.login.RequestSignInModel;
import com.cyclecompany.bestdiscount.model.news.NewsData;
import com.cyclecompany.bestdiscount.model.photo.PhotoData;
import com.cyclecompany.bestdiscount.model.pincode.PinCodeData;
import com.cyclecompany.bestdiscount.model.referals.ReferalsData;
import com.cyclecompany.bestdiscount.model.register.RegistrationData;
import com.cyclecompany.bestdiscount.model.register.RequestSignUpModel;
import com.cyclecompany.bestdiscount.model.shop_history.ShopHistoryData;
import com.cyclecompany.bestdiscount.model.shops.AllShopsData;
import com.cyclecompany.bestdiscount.model.specialnews.SpecialNewsData;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import java.io.File;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Single instance class for server requests.
 */

public class DataService {

    private static DataService dataService;

    public static DataService init() {
        if (dataService == null) {
            dataService = new DataService();
        }
        return dataService;
    }

    public void signUpUser(Context activity, final onRequestSignUpResult onRequestSignUpResult, String username, String email,
                           String password, String passwordConfirm, String cookie) {
        ServerApiManager.getApiService().signUpUser(new RequestSignUpModel(username, email, password, passwordConfirm), new Callback<RegistrationData>() {
            @Override
            public void success(RegistrationData registrationData, Response response) {

                onRequestSignUpResult.onRequestSignUpResult(registrationData,
                        "");
            }

            @Override
            public void failure(RetrofitError error) {
                int a = 5;
            }
        });
    }

    public void signInUser(Context activity, final onRequestSignInResult onRequestSignInResult, String phone,
                           String password, String cookie) {
        ServerApiManager.getApiService().signInUser(new RequestSignInModel(password, phone), new Callback<LoginData>() {
            @Override
            public void success(LoginData loginData, Response response) {
                onRequestSignInResult.onRequestSignInResult(loginData, "");
            }

            @Override
            public void failure(RetrofitError error) {
                int a = 5;
            }
        });
    }


    public void signInUserWithToken(Context activity, final onRequestSignInResult onRequestSignInResult, String phone,
                                    String token, String cookie) {
        ServerApiManager.getApiService().signInUserWithToken(new RequestSignInModel(phone, token, false), new Callback<LoginData>() {
            @Override
            public void success(LoginData loginData, Response response) {
                onRequestSignInResult.onRequestSignInResult(loginData, response.getHeaders().get(6).getValue());
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    public void getReferalInviteCode(Activity activity, final onRequestReferalCodeResult listener, String cookie) {

        ServerApiManager.getApiService().getReferalCodeForFriend(activity.getString(R.string.mobile_api),
                activity.getString(R.string.get_referal_code_api),
                activity.getString(R.string.test_token_api),
                new Callback<FriendInviteCode>() {
                    @Override
                    public void success(FriendInviteCode friendInviteCode, Response response) {
                        listener.onRequestReferalCodeResult(friendInviteCode, response.getHeaders().get(6).getValue());
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                }
        );
    }

    public void getShopHistory(Activity activity, final onRequestHistoryResult listener, String userid, String token) {

        ServerApiManager.getApiService().getShopHistory(userid, token, new Callback<ShopHistoryData>() {
            @Override
            public void success(ShopHistoryData shopHistoryData, Response response) {
                listener.onRequestHistoryResylt(shopHistoryData);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    public void getSpecialNews(final onRequestSpecialNewsResult listener, Realm realm) {

        ServerApiManager.getApiService().getSpecialNews(RealmUtils.getUserSettings(realm).getToken(), new Callback<SpecialNewsData>() {
            @Override
            public void success(SpecialNewsData specialNewsData, Response response) {
                listener.onRequestSpecialNewsResult(specialNewsData, response.getHeaders().get(6).getValue());
            }

            @Override
            public void failure(RetrofitError error) {
                int a = 5;
            }
        });
    }

    public void getNews(final onRequestNewsResult listener, final String token) {
        ServerApiManager.getApiService().getNews(token, new Callback<NewsData>() {
            @Override
            public void success(NewsData newsData, Response response) {
                listener.onRequestNewsResult(newsData);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getAllShops(final onRequestAllShopsResult listener, String token) {

        ServerApiManager.getApiService().getAllShops(token, new Callback<AllShopsData>() {
            @Override
            public void success(AllShopsData shopsData, Response response) {
                listener.onRequestAllShopsResult(shopsData);
            }

            @Override
            public void failure(RetrofitError error) {
                int a = 5;
            }
        });
    }


    public void getPincodeShop(final onRequestPincodeResult listener, String userid, String token) {

        ServerApiManager.getApiService().getPincode(userid, token, new Callback<PinCodeData>() {
            @Override
            public void success(PinCodeData pinCodeData, Response response) {
                listener.onRequestPincodeResult(pinCodeData);
            }

            @Override
            public void failure(RetrofitError error) {
                int a = 5;
            }
        });
    }

    public void getUserInfo(final onRequestSignInResult listener, String userId, String token) {
        ServerApiManager.getApiService().getUserInfo(userId, token, new Callback<LoginData>() {
            @Override
            public void success(LoginData loginData, Response response) {
                listener.onRequestSignInResult(loginData, "");
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void sendPhoto(File file, final onRequestPhotoResult listener, String token) {
        TypedFile typedFile = new TypedFile("image/jpeg", file);

        ServerApiManager.getApiService().uploadFile(typedFile, new Callback<PhotoData>() {
            @Override
            public void success(PhotoData photoData, Response response) {
                listener.onRequestPhotoResult(photoData);
            }

            @Override
            public void failure(RetrofitError error) {
                int a = 5;
            }
        });
    }

    public void getUserReferals(final onRequestReferalsResult listener, String userId, String token) {
        ServerApiManager.getApiService().getReferals(userId, token, new Callback<ReferalsData>() {
            @Override
            public void success(ReferalsData referalsData, Response response) {
                listener.onRequestReferalsResult(referalsData);
            }

            @Override
            public void failure(RetrofitError error) {
                int a = 5;
            }
        });
    }

    public void getShopCategories(final onRequestCategoriesResult listener, String token) {

        ServerApiManager.getApiService().getCategories(token, new Callback<CategoriesData>() {
            @Override
            public void success(CategoriesData categoriesData, Response response) {
                listener.onRequestCategoriesResult(categoriesData);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void sendAnketa(final onRequestAnketaResult listener, int userId, String token, AnketaPostData data) {

        ServerApiManager.getApiService().sendUserAnketa(token, data, new Callback<PhotoData>() {
            @Override
            public void success(PhotoData photoData, Response response) {
                listener.onRequestAnketaResult(photoData);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public interface onRequestSpecialNewsResult {
        public void onRequestSpecialNewsResult(SpecialNewsData specialNewsData, String cookie);

    }

    public interface onRequestNewsResult {
        public void onRequestNewsResult(NewsData specialNewsData);

    }

    public interface onRequestPincodeResult {
        public void onRequestPincodeResult(PinCodeData data);

    }

    public interface onRequestReferalCodeResult {
        public void onRequestReferalCodeResult(FriendInviteCode code, String cookie);
    }

    public interface onRequestSignUpResult {
        public void onRequestSignUpResult(RegistrationData responseMessage, String cookie);
    }

    public interface onRequestSignInResult {
        public void onRequestSignInResult(LoginData responseMessage, String cookie);
    }

    public interface onRequestAllShopsResult {

        public void onRequestAllShopsResult(AllShopsData shopsData);

    }

    public interface onRequestHistoryResult {

        public void onRequestHistoryResylt(ShopHistoryData shopHistoryData);

    }

    public interface onRequestReferalsResult {

        public void onRequestReferalsResult(ReferalsData referalsData);

    }

    public interface onRequestPhotoResult {
        public void onRequestPhotoResult(PhotoData photoData);
    }

    public interface onRequestCategoriesResult {
        public void onRequestCategoriesResult(CategoriesData categoriesData);
    }

    public interface onRequestAnketaResult {
        public void onRequestAnketaResult(PhotoData photoData);
    }
}

