package com.cyclecompany.bestdiscount.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.adapters.ReferalsRVAdapter;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.referals.ReferalsData;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Илья on 15.09.2016.
 */

public class ReferalsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.rv)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    public ReferalsFragment() {
    }

    public static ReferalsFragment newInstance() {
        ReferalsFragment newsFragment = new ReferalsFragment();
        return newsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_news, container, false);

        initViewElements(convertView);

        getReferalsToLayout();

        return convertView;
    }

    private void initViewElements(View convertView) {
        ButterKnife.bind(this, convertView);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        mRecyclerView.setLayoutManager(layoutManager);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {

        getReferalsToLayout();

    }

    private void getReferalsToLayout(){

        progressBar.setVisibility(View.GONE);

        DataService.init().getUserReferals(new DataService.onRequestReferalsResult() {
            @Override
            public void onRequestReferalsResult(ReferalsData referalsData) {

                ReferalsRVAdapter refAdapter = new ReferalsRVAdapter(getActivity(),referalsData.getData(),false);
                mRecyclerView.setAdapter(refAdapter);
            }
        }, RealmUtils.getUserSettings(BaseActivity.realm).getUserModelRealm().getId(),
                RealmUtils.getUserSettings(BaseActivity.realm).getUserModelRealm().getToken()
                );



    }
}
