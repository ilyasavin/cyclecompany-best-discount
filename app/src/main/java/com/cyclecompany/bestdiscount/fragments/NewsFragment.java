package com.cyclecompany.bestdiscount.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.adapters.NewsRVAdapter;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.news.NewsData;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.View.GONE;

/**
 * Created by Илья on 15.09.2016.
 */

public class NewsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    @Bind(R.id.rv)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    public NewsFragment() {
    }

    public static NewsFragment newInstance() {
        NewsFragment newsFragment = new NewsFragment();
        return newsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_spec_news, container, false);

        initViewElements(convertView);

        getNewsToLayout();

        return convertView;
    }

    private void initViewElements(View convertView) {
        ButterKnife.bind(this, convertView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        mRecyclerView.setLayoutManager(layoutManager);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {

        getNewsToLayout();

    }

    private void getNewsToLayout(){

        progressBar.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(true);

       getNews();


    }

    private void getNews() {
        DataService.init().getNews(new DataService.onRequestNewsResult() {
            @Override
            public void onRequestNewsResult(NewsData specialNewsData) {

                NewsRVAdapter newsRVAdapter = new NewsRVAdapter(getActivity(), specialNewsData.getData());

                mRecyclerView.setAdapter(newsRVAdapter);

                mSwipeRefreshLayout.setRefreshing(false);

                progressBar.setVisibility(GONE);
            }
        }, RealmUtils.getUserSettings(BaseActivity.realm).getToken());
    }
}
