package com.cyclecompany.bestdiscount.fragments;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.login.LoginData;
import com.cyclecompany.bestdiscount.model.register.UserModel;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;
import com.cyclecompany.bestdiscount.util.PrefUtil;
import com.cyclecompany.bestdiscount.util.RealmUtils;


public class AuthorizationFragment extends Fragment {

    private EditText inputPhone, inputPassword;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;

    public AuthorizationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_authorization, container, false);


        inputLayoutEmail = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) view.findViewById(R.id.input_layout_password);

        inputPhone = (EditText) view.findViewById(R.id.input_email);
        inputPassword = (EditText) view.findViewById(R.id.input_password);
        Button signIn = (Button) view.findViewById(R.id.btn_sign_in);

        inputPhone.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_PHONE);

        inputPhone.addTextChangedListener(new MyTextWatcher(inputPhone));
        inputPassword.addTextChangedListener(new MyTextWatcher(inputPassword));


        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    submitForm();

            }
        });

        return view;
    }

    private void signInRest() {

        DataService.init().signInUser(getActivity(), new DataService.onRequestSignInResult() {
            @Override
            public void onRequestSignInResult(LoginData responseMessage, String cookie) {
                if(responseMessage.getMessage().getCode() == 1){
                    UserModel model = responseMessage.getData().get(0);
                    RealmUtils.addUserSettings(BaseActivity.realm,
                                    model);
                    PrefUtil.setIsAuthorized(getActivity());

                    ActivityNavigator.startAnketa(getActivity());

                    getActivity().finish();
                }
                else Toast.makeText(getActivity(), responseMessage.getMessage().getValue(), Toast.LENGTH_SHORT).show();
            }

        },inputPhone.getText().toString(),inputPassword.getText().toString(),
                "");
    }

    private boolean submitForm() {


         if (!validatePassword()||!validatePhone()) {
         return false;
         }
            else {
             signInRest();
             return true;
         }
    }

    private boolean validatePhone() {
        String email = inputPhone.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError("Неверный формат номера");
            requestFocus(inputPhone);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError("Ошибка пароля");
            requestFocus(inputPassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.PHONE.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }
}