package com.cyclecompany.bestdiscount.fragments;


import android.app.Fragment;
import android.widget.ProgressBar;

import com.cyclecompany.bestdiscount.R;

import butterknife.Bind;

/**
 * Created by ilyas on 6/28/2016.
 */

public abstract class BaseFragment extends Fragment {


    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    public void onStart() {
        super.onStart();

    }


}
