package com.cyclecompany.bestdiscount.fragments;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.pincode.PinCodeData;
import com.cyclecompany.bestdiscount.model.realm.SettingsObject;
import com.cyclecompany.bestdiscount.util.RealmUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class UserBarCodeDialogFragment extends DialogFragment implements View.OnClickListener {


    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    @Bind(R.id.barCodeView)
    ImageView barCodeView;
    SettingsObject mSettingsObject;

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.dialog_fragment, null);

        int width = 200;
        int height = 200;
        getDialog().getWindow().setLayout(width, height);


        ButterKnife.bind(this, v);

        mSettingsObject = RealmUtils.getUserSettings(BaseActivity.realm);


        getPincode();

        Toast.makeText(getActivity(), "Получите консультацию у участника системы!", Toast.LENGTH_SHORT).show();


        return v;
    }

    public void onClick(View v) {

        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private void getPincode() {
        DataService.init().getPincodeShop(new DataService.onRequestPincodeResult() {
            @Override
            public void onRequestPincodeResult(PinCodeData data) {

                try {
                    Bitmap bitmap = encodeAsBitmap(String.valueOf(data.getData().get(0).getPincode()), BarcodeFormat.CODE_128, 400, 200);
                    barCodeView.setImageBitmap(bitmap);
                } catch (WriterException e) {
                    e.printStackTrace();
                }


            }
        }, mSettingsObject.getUserModelRealm().getId(), mSettingsObject.getUserModelRealm().getToken());
    }


}