package com.cyclecompany.bestdiscount.fragments;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.model.realm.SettingsObject;
import com.cyclecompany.bestdiscount.util.RealmUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;


public class InviteReferalDialogFragment extends DialogFragment implements View.OnClickListener {


  @Bind(R.id.barCodeView)
  ImageView barCodeView;

  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {



    View v = inflater.inflate(R.layout.dialog_fragment, null);

    int width = 200;
    int height = 200;
    getDialog().getWindow().setLayout(width, height);


    ButterKnife.bind(this, v);

    SettingsObject mSettingsObject = RealmUtils.getUserSettings(BaseActivity.realm);

    Toast.makeText(getActivity(), "Получите консультацию у участника системы!", Toast.LENGTH_SHORT).show();

    try {
      Bitmap bitmap = encodeAsBitmap(mSettingsObject.getUserModelRealm().getId());
      barCodeView.setImageBitmap(bitmap);
    } catch (WriterException e) {
      e.printStackTrace();
    }


    return v;
  }
 
  public void onClick(View v) {

    dismiss();
  }
 
  public void onDismiss(DialogInterface dialog) {
    super.onDismiss(dialog);

  }
 
  public void onCancel(DialogInterface dialog) {
    super.onCancel(dialog);

  }

  Bitmap encodeAsBitmap(String str) throws WriterException {
    BitMatrix result;
    try {
      result = new MultiFormatWriter().encode(str,
              BarcodeFormat.QR_CODE, 100, 100, null);
    } catch (IllegalArgumentException iae) {
      // Unsupported format
      return null;
    }
    int w = result.getWidth();
    int h = result.getHeight();
    int[] pixels = new int[w * h];
    for (int y = 0; y < h; y++) {
      int offset = y * w;
      for (int x = 0; x < w; x++) {
        pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
      }
    }
    Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
    bitmap.setPixels(pixels, 0, 100, 0, 0, w, h);
    return bitmap;
  }


}