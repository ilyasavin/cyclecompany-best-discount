package com.cyclecompany.bestdiscount.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.adapters.CategoriesRVAdapter;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.categories.CategoriesData;
import com.cyclecompany.bestdiscount.util.RealmUtils;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.View.GONE;

/**
 * Created by Илья on 15.09.2016.
 */

public class ShopFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.rv)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;


    public ShopFragment() {
    }

    public static ShopFragment newInstance() {
        ShopFragment newsFragment = new ShopFragment();
        return newsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_spec_news, container, false);

        initViewElements(convertView);

        getCategoriesToLayout();

        return convertView;
    }

    private void initViewElements(View convertView) {
        ButterKnife.bind(this, convertView);
        int spanCount = 6; // 3 columns
        int spacing = 50; // 50px
        boolean includeEdge = false;
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.setAlignment(Alignment.LEFT);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {

        getCategoriesToLayout();

    }

    private void getCategoriesToLayout() {

        progressBar.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(true);

        getCategories();


    }

    private void getCategories() {
        DataService.init().getShopCategories(new DataService.onRequestCategoriesResult() {
            @Override
            public void onRequestCategoriesResult(CategoriesData categoriesData) {

                CategoriesRVAdapter adapter = new CategoriesRVAdapter(getActivity(),
                        categoriesData.getData().get(0));

                mRecyclerView.setAdapter(adapter);

                mSwipeRefreshLayout.setRefreshing(false);

                progressBar.setVisibility(GONE);
            }
        }, RealmUtils.getUserSettings(BaseActivity.realm).getToken());

    }
}