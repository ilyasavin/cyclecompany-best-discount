package com.cyclecompany.bestdiscount.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.components.Constants;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.login.LoginData;
import com.cyclecompany.bestdiscount.model.realm.SettingsObject;
import com.cyclecompany.bestdiscount.model.realm.UserModelRealm;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;
import com.cyclecompany.bestdiscount.util.RealmUtils;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by Илья on 15.09.2016.
 */

public class UserFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static CircularImageView userPhoto;
    ImageView barCodeView;
    @Bind(R.id.userProfileText)
    TextView userNameText;
    @Bind(R.id.userProfilePhone)
    TextView userPhoneText;
    @Bind(R.id.userProfileReferalsCountText)
    TextView userReferalsText;
    @Bind(R.id.userProfileScores)
    TextView userProfileScoresText;
    @Bind(R.id.showCode)
    Button showCodeBtn;
    @Bind(R.id.qrBtn)
    Button qrBtn;
    @Bind(R.id.showHistory)
    Button historyBtn;
    @Bind(R.id.userScode)
    TextView userScore;
    SettingsObject mSettingsObject;

    public static String url;

    public UserFragment() {
    }

    public static UserFragment newInstance() {
        UserFragment newsFragment = new UserFragment();
        return newsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_user, container, false);

        initViewElements(convertView);

        userPhoto = (CircularImageView) convertView.findViewById(R.id.img_staff_face);

        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();

        mSettingsObject = RealmUtils.getUserSettings(BaseActivity.realm);

        barCodeView = (ImageView) convertView.findViewById(R.id.barCodeView);


        qrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment mDialogFragment;
                mDialogFragment = new InviteReferalDialogFragment();
                mDialogFragment.show(getFragmentManager(), Constants.SELECT_CITY_DIALOG);
            }
        });
        showCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment mDialogFragment;
                mDialogFragment = new UserBarCodeDialogFragment();
                mDialogFragment.show(getFragmentManager(), Constants.SELECT_CITY_DIALOG);
            }
        });

        userNameText.setText(mSettingsObject.getUserModelRealm().getUsername());
        userPhoneText.setText(mSettingsObject.getUserModelRealm().getEmail());
        userProfileScoresText.setText(mSettingsObject.getUserModelRealm().getId());
        UserModelRealm user = mSettingsObject.getUserModelRealm();
        userScore.setText(mSettingsObject.getUserModelRealm().getPoints() + " баллов");

        historyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityNavigator.startShopHistoryActivity(getActivity());
            }
        });

        DataService.init().getUserInfo(new DataService.onRequestSignInResult() {
            @Override
            public void onRequestSignInResult(LoginData responseMessage, String cookie) {
                userScore.setText(responseMessage.getData().get(0).getPoints());
                userReferalsText.setText(responseMessage.getData().get(0).getTeamPoints());
            }
        }, mSettingsObject.getUserModelRealm().getId(), mSettingsObject.getToken());

        userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EasyImage.openCamera(getActivity(), 0);

            }
        });


        return convertView;
    }

    private void initViewElements(View convertView) {

        ButterKnife.bind(this, convertView);


    }

    @Override
    public void onRefresh() {

        getNewsToLayout();

    }

    private void getNewsToLayout() {

        progressBar.setVisibility(View.VISIBLE);

    }


}
