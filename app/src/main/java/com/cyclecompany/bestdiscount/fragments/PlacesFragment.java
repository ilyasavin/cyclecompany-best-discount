package com.cyclecompany.bestdiscount.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cyclecompany.bestdiscount.BaseActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.adapters.AllShopsRVAdapter;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.shops.AllShopsData;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.View.GONE;

/**
 * Created by Илья on 15.09.2016.
 */

public class PlacesFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    @Bind(R.id.rv)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    public PlacesFragment() {
    }

    public static PlacesFragment newInstance() {
        PlacesFragment newsFragment = new PlacesFragment();
        return newsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_shops, container, false);

        initViewElements(convertView);

        getNewsToLayout();

        return convertView;
    }

    private void initViewElements(View convertView) {
        ButterKnife.bind(this, convertView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {

        getNewsToLayout();

    }

    private void getNewsToLayout() {

        progressBar.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(true);

        getPlaces();


    }

    private void getPlaces() {
        DataService.init().getAllShops(new DataService.onRequestAllShopsResult() {
            @Override
            public void onRequestAllShopsResult(AllShopsData shopsData) {

                AllShopsRVAdapter adapter = new AllShopsRVAdapter(getActivity(), shopsData.getData());

                mSwipeRefreshLayout.setRefreshing(false);

                progressBar.setVisibility(GONE);

                mRecyclerView.setAdapter(adapter);
                int a = 5;
            }
        }, RealmUtils.getUserSettings(BaseActivity.realm).getToken());
    }
}