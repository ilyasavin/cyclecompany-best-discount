package com.cyclecompany.bestdiscount.adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.model.specialnews.SpecialNewsModel;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.View.GONE;

/**
 * An adapter for the list of News
 */
public class NewsShopRVAdapter extends RecyclerView.Adapter<NewsShopRVAdapter.ViewHolder> {

    private List<SpecialNewsModel> newsList;
    private final Context context;
    private int lastPosition = -1;

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    public NewsShopRVAdapter(Context context, List<SpecialNewsModel> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_post, viewGroup, false);
        return new ViewHolder(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        if(newsList.get(i).getText().equals(""))
            viewHolder.postText.setVisibility(GONE);

        viewHolder.postText.setText(newsList.get(i).getText());
        viewHolder.postDateText.setText(newsList.get(i).getTime());
        if(newsList.get(i).getImage()!=null)
        if(newsList.get(i).getImage()!="")
            Picasso.with(context).load(newsList.get(i).getImage()).into(viewHolder.imageView);


    }

    private void addAnimationToPostItem(ViewHolder viewHolder, int i) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (i > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_to_top);
        viewHolder.itemView.startAnimation(animation);
        lastPosition = i;
    }



    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.post_container)
        CardView cardView;
        @Bind(R.id.tv_name)
        TextView postText;
        @Bind(R.id.timestamp)
        RelativeTimeTextView postDateText;
        @Bind(R.id.img_news)
        ImageView imageView;


        protected Context context;

        public ViewHolder(View view, final Context context) {
            super(view);

            ButterKnife.bind(this, view);
            postText.setOnClickListener(this);
            this.context = context;
        }



        @Override
        public void onClick(View v) {

            if(v==postText){

            }

        }

    }
}