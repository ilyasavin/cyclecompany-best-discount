package com.cyclecompany.bestdiscount.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.model.referals.ReferalModel;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * An adapter for the list of Referals
 */
public class ReferalsRVAdapter extends RecyclerView.Adapter<ReferalsRVAdapter.ViewHolder> {

    private final Context context;
    boolean adapterCode;
    private List<ReferalModel> userList;
    private int lastPosition = -1;

    public ReferalsRVAdapter(Context context, List<ReferalModel> userModel, boolean adapter) {
        this.context = context;
        this.userList = userModel;
        this.adapterCode = adapter;
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_referal, viewGroup, false);
        return new ViewHolder(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {


        viewHolder.postText.setText(userList.get(i).getUsername());

        if (userList.get(i).getUserphoto() != null && !Objects.equals(userList.get(i).getUserphoto(), ""))
            Picasso.with(context).load("http://thebest-d.com/new" + userList.get(i).getUserphoto()).into(viewHolder.imageView);


    }

    private void addAnimationToPostItem(ViewHolder viewHolder, int i) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (i > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_to_top);
        viewHolder.itemView.startAnimation(animation);
        lastPosition = i;
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected Context context;
        @Bind(R.id.tv_name)
        TextView postText;
        @Bind(R.id.img_staff_face)
        CircularImageView imageView;
        @Bind(R.id.referalContainer)
        ViewGroup referalContainer;

        public ViewHolder(View view, final Context context) {
            super(view);

            ButterKnife.bind(this, view);
            postText.setOnClickListener(this);
            referalContainer.setOnClickListener(this);
            this.context = context;

        }


        @Override
        public void onClick(View v) {

            if ( v == referalContainer)

            {
                ActivityNavigator.startReferalActivity((Activity)context, userList.get(getPosition()),imageView);
            }
        }

    }
}