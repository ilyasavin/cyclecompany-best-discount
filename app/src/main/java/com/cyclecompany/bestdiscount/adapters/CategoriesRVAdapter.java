package com.cyclecompany.bestdiscount.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.cyclecompany.bestdiscount.MainActivity;
import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.fragments.PlacesFragment;
import com.cyclecompany.bestdiscount.model.categories.CategoryItem;
import com.cyclecompany.bestdiscount.navigator.FragmentNavigator;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * An adapter for the list of Referals
 */
public class CategoriesRVAdapter extends RecyclerView.Adapter<CategoriesRVAdapter.ViewHolder> {

    private final Context context;
    private List<CategoryItem> categoryItems;
    private int lastPosition = -1;

    public CategoriesRVAdapter(Context context, List<CategoryItem> userModel) {
        this.context = context;
        this.categoryItems = userModel;
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_categoryl, viewGroup, false);
        return new ViewHolder(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {


        viewHolder.postText.setText(categoryItems.get(i).getValue());


    }

    private void addAnimationToPostItem(ViewHolder viewHolder, int i) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (i > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_to_top);
        viewHolder.itemView.startAnimation(animation);
        lastPosition = i;
    }


    @Override
    public int getItemCount() {
        return categoryItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected Context context;
        @Bind(R.id.tv_name)
        TextView postText;
        @Bind(R.id.card)
        CardView card;

        public ViewHolder(View view, final Context context) {
            super(view);

            ButterKnife.bind(this, view);
            card.setOnClickListener(this);
            this.context = context;

        }


        @Override
        public void onClick(View v) {

            if (v == card)

            {
                MainActivity.mTitleText.setText(categoryItems.get(getPosition()).getValue());
                FragmentNavigator.showContentFragment((Activity) context, PlacesFragment.newInstance());
            }
        }

    }
}