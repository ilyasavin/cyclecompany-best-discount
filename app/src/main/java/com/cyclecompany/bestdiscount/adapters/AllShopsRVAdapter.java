package com.cyclecompany.bestdiscount.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.model.shops.Shop;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * An adapter for the list of News
 */
public class AllShopsRVAdapter extends RecyclerView.Adapter<AllShopsRVAdapter.ViewHolder> {

    private final Context context;
    private List<Shop> newsList;
    private int lastPosition = -1;

    public AllShopsRVAdapter(Context context, List<Shop> newsList) {
        this.context = context;
        this.newsList = newsList;


        for (int i = 0; i < newsList.size(); i++) {
            if (newsList.get(i).getUserphoto() == null || newsList.get(i).getName() == null
                    || newsList.get(i).getUsername() == null || newsList.get(i).getUsername().equals(""))
                newsList.remove(i);

        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_shop, viewGroup, false);
        return new ViewHolder(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        if (newsList.get(i).getName() == null || newsList.get(i).getName().equals(""))
            viewHolder.postText.setText(newsList.get(i).getUsername());
        else
            viewHolder.postText.setText(newsList.get(i).getName());

        if (newsList.get(i).getUserphoto() != null)
            if (newsList.get(i).getUserphoto() != "")
                Picasso.with(context).load(newsList.get(i).getUserphoto()).into(viewHolder.imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        viewHolder.imageView.setVisibility(View.GONE);
                    }
                });


    }

    private void addAnimationToPostItem(ViewHolder viewHolder, int i) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (i > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_to_top);
        viewHolder.itemView.startAnimation(animation);
        lastPosition = i;
    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected Context context;
        @Bind(R.id.post_container)
        CardView cardView;
        @Bind(R.id.tv_name)
        TextView postText;
        @Bind(R.id.img_news)
        ImageView imageView;

        public ViewHolder(View view, final Context context) {
            super(view);

            ButterKnife.bind(this, view);
            cardView.setOnClickListener(this);
            this.context = context;
        }


        @Override
        public void onClick(View v) {

            if (v == cardView) {

                ActivityNavigator.startShopActivity((Activity)context, newsList.get(getPosition()),imageView);

            }

        }

    }
}