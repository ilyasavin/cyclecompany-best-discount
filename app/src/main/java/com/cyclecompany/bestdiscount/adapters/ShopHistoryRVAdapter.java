package com.cyclecompany.bestdiscount.adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.cyclecompany.bestdiscount.R;
import com.cyclecompany.bestdiscount.model.shop_history.ShopHistory;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * An adapter for the list of News
 */
public class ShopHistoryRVAdapter extends RecyclerView.Adapter<ShopHistoryRVAdapter.ViewHolder> {

    private final Context context;
    private List<ShopHistory> newsList;
    private int lastPosition = -1;

    public ShopHistoryRVAdapter(Context context, List<ShopHistory> newsList) {
        this.context = context;
        this.newsList = newsList;


        }


    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_purchase, viewGroup, false);
        return new ViewHolder(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {


        viewHolder.postText.setText(newsList.get(i).getPurchaseName());
        viewHolder.price.setText(newsList.get(i).getPrice());


    }

    private void addAnimationToPostItem(ViewHolder viewHolder, int i) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (i > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_to_top);
        viewHolder.itemView.startAnimation(animation);
        lastPosition = i;
    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected Context context;
        @Bind(R.id.post_container)
        CardView cardView;
        @Bind(R.id.tv_name)
        TextView postText;
        @Bind(R.id.tv_price)
        TextView price;

        public ViewHolder(View view, final Context context) {
            super(view);

            ButterKnife.bind(this, view);
            cardView.setOnClickListener(this);
            this.context = context;
        }


        @Override
        public void onClick(View v) {

            if (v == cardView) {
            }

        }

    }
}