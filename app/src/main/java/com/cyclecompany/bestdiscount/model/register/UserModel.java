
package com.cyclecompany.bestdiscount.model.register;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class UserModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("team_points")
    @Expose
    private String teamPoints;
    @SerializedName("deposit")
    @Expose
    private String deposit;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("smscode")
    @Expose
    private String smscode;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("auth_key")
    @Expose
    private String authKey;
    @SerializedName("password_hash")
    @Expose
    private String passwordHash;
    @SerializedName("password_reset_token")
    @Expose
    private String passwordResetToken;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("userphoto")
    @Expose
    private String userphoto;
    @SerializedName("thrumbnail")
    @Expose
    private String thrumbnail;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("time_zone")
    @Expose
    private String timeZone;
    @SerializedName("recovery_key")
    @Expose
    private String recoveryKey;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;


    public UserModel(String id,
                          String email,
                          Integer status, Integer type,
                          String points, String username,
                          String phone, String smscode,
                          String token) {
        this.id = id;
        this.email = email;
        this.status = status;
        this.type = type;
        this.points = points;
        this.username = username;
        this.phone = phone;
        this.smscode = smscode;
        this.token = token;
    }

    /**
     * 
     * @return
     *     The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 
     * @param level
     *     The level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * 
     * @return
     *     The points
     */
    public String getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The teamPoints
     */
    public String getTeamPoints() {
        return teamPoints;
    }

    /**
     * 
     * @param teamPoints
     *     The team_points
     */
    public void setTeamPoints(String teamPoints) {
        this.teamPoints = teamPoints;
    }

    /**
     * 
     * @return
     *     The deposit
     */
    public String getDeposit() {
        return deposit;
    }

    /**
     * 
     * @param deposit
     *     The deposit
     */
    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    /**
     * 
     * @return
     *     The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *     The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The smscode
     */
    public String getSmscode() {
        return smscode;
    }

    /**
     * 
     * @param smscode
     *     The smscode
     */
    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }

    /**
     * 
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return
     *     The token
     */
    public String getToken() {
        return token;
    }

    /**
     * 
     * @param token
     *     The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The authKey
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * 
     * @param authKey
     *     The auth_key
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    /**
     * 
     * @return
     *     The passwordHash
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * 
     * @param passwordHash
     *     The password_hash
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * 
     * @return
     *     The passwordResetToken
     */
    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    /**
     * 
     * @param passwordResetToken
     *     The password_reset_token
     */
    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    /**
     * 
     * @return
     *     The role
     */
    public String getRole() {
        return role;
    }

    /**
     * 
     * @param role
     *     The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The userphoto
     */
    public String getUserphoto() {
        return userphoto;
    }

    /**
     * 
     * @param userphoto
     *     The userphoto
     */
    public void setUserphoto(String userphoto) {
        this.userphoto = userphoto;
    }

    /**
     * 
     * @return
     *     The thrumbnail
     */
    public String getThrumbnail() {
        return thrumbnail;
    }

    /**
     * 
     * @param thrumbnail
     *     The thrumbnail
     */
    public void setThrumbnail(String thrumbnail) {
        this.thrumbnail = thrumbnail;
    }

    /**
     * 
     * @return
     *     The pincode
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * 
     * @param pincode
     *     The pincode
     */
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * 
     * @return
     *     The timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * 
     * @param timeZone
     *     The time_zone
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * 
     * @return
     *     The recoveryKey
     */
    public String getRecoveryKey() {
        return recoveryKey;
    }

    /**
     * 
     * @param recoveryKey
     *     The recovery_key
     */
    public void setRecoveryKey(String recoveryKey) {
        this.recoveryKey = recoveryKey;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.status);
        dest.writeValue(this.type);
        dest.writeValue(this.level);
        dest.writeString(this.points);
        dest.writeString(this.teamPoints);
        dest.writeString(this.deposit);
        dest.writeString(this.username);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.smscode);
        dest.writeString(this.password);
        dest.writeString(this.token);
        dest.writeString(this.timestamp);
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.authKey);
        dest.writeString(this.passwordHash);
        dest.writeString(this.passwordResetToken);
        dest.writeString(this.role);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.userphoto);
        dest.writeString(this.thrumbnail);
        dest.writeString(this.pincode);
        dest.writeString(this.timeZone);
        dest.writeString(this.recoveryKey);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
    }

    protected UserModel(Parcel in) {
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.level = (Integer) in.readValue(Integer.class.getClassLoader());
        this.points = in.readString();
        this.teamPoints = in.readString();
        this.deposit = in.readString();
        this.username = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.smscode = in.readString();
        this.password = in.readString();
        this.token = in.readString();
        this.timestamp = in.readString();
        this.id = in.readString();
        this.name = in.readString();
        this.authKey = in.readString();
        this.passwordHash = in.readString();
        this.passwordResetToken = in.readString();
        this.role = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.userphoto = in.readString();
        this.thrumbnail = in.readString();
        this.pincode = in.readString();
        this.timeZone = in.readString();
        this.recoveryKey = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
    }

    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
