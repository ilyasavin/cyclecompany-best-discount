package com.cyclecompany.bestdiscount.model.anketa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnketaPostData {

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("cars")
    @Expose
    private String cars;
    @SerializedName("art")
    @Expose
    private String art;
    @SerializedName("it")
    @Expose
    private String it;
    @SerializedName("health")
    @Expose
    private String health;
    @SerializedName("neededucation")
    @Expose
    private String neededucation;
    @SerializedName("clothes")
    @Expose
    private String clothes;
    @SerializedName("food")
    @Expose
    private String food;
    @SerializedName("sport")
    @Expose
    private String sport;
    @SerializedName("home")
    @Expose
    private String home;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("games")
    @Expose
    private String games;
    @SerializedName("sushi")
    @Expose
    private String sushi;
    @SerializedName("pizza")
    @Expose
    private String pizza;
    @SerializedName("nightclub")
    @Expose
    private String nightclub;
    @SerializedName("sauna")
    @Expose
    private String sauna;
    @SerializedName("drink")
    @Expose
    private String drink;
    @SerializedName("mychose")
    @Expose
    private String mychose;
    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("car")
    @Expose
    private String car;
    @SerializedName("education")
    @Expose
    private String education;

    public AnketaPostData(Integer userid, String cars, String it, String art,
                          String health, String neededucation, String clothes,
                          String food, String sport, String home, String video,
                          String sushi, String games, String pizza,
                          String sauna, String nightclub, String drink, String mychose,
                          String family, String car, String education) {
        this.userid = userid;
        this.cars = cars;
        this.it = it;
        this.art = art;
        this.health = health;
        this.neededucation = neededucation;
        this.clothes = clothes;
        this.food = food;
        this.sport = sport;
        this.home = home;
        this.video = video;
        this.sushi = sushi;
        this.games = games;
        this.pizza = pizza;
        this.sauna = sauna;
        this.nightclub = nightclub;
        this.drink = drink;
        this.mychose = mychose;
        this.family = family;
        this.car = car;
        this.education = education;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getCars() {
        return cars;
    }

    public void setCars(String cars) {
        this.cars = cars;
    }

    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getNeededucation() {
        return neededucation;
    }

    public void setNeededucation(String neededucation) {
        this.neededucation = neededucation;
    }

    public String getClothes() {
        return clothes;
    }

    public void setClothes(String clothes) {
        this.clothes = clothes;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getGames() {
        return games;
    }

    public void setGames(String games) {
        this.games = games;
    }

    public String getSushi() {
        return sushi;
    }

    public void setSushi(String sushi) {
        this.sushi = sushi;
    }

    public String getPizza() {
        return pizza;
    }

    public void setPizza(String pizza) {
        this.pizza = pizza;
    }

    public String getNightclub() {
        return nightclub;
    }

    public void setNightclub(String nightclub) {
        this.nightclub = nightclub;
    }

    public String getSauna() {
        return sauna;
    }

    public void setSauna(String sauna) {
        this.sauna = sauna;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getMychose() {
        return mychose;
    }

    public void setMychose(String mychose) {
        this.mychose = mychose;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

}