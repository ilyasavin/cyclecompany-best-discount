
package com.cyclecompany.bestdiscount.model.referals;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferalsData {

    @SerializedName("data")
    @Expose
    private List<ReferalModel> data = null;
    @SerializedName("message")
    @Expose
    private Message message;

    public List<ReferalModel> getData() {
        return data;
    }

    public void setData(List<ReferalModel> data) {
        this.data = data;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

}
