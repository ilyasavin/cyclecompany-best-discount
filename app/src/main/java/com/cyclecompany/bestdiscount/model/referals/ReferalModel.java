
package com.cyclecompany.bestdiscount.model.referals;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferalModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("auth_key")
    @Expose
    private String authKey;
    @SerializedName("password_hash")
    @Expose
    private String passwordHash;
    @SerializedName("password_reset_token")
    @Expose
    private String passwordResetToken;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("userphoto")
    @Expose
    private String userphoto;
    @SerializedName("thrumbnail")
    @Expose
    private String thrumbnail;
    @SerializedName("smscode")
    @Expose
    private String smscode;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("time_zone")
    @Expose
    private String timeZone;
    @SerializedName("recovery_key")
    @Expose
    private String recoveryKey;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("team_points")
    @Expose
    private String teamPoints;
    @SerializedName("deposit")
    @Expose
    private String deposit;
    @SerializedName("categories")
    @Expose
    private String categories;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("sponsor")
    @Expose
    private String sponsor;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("okrug")
    @Expose
    private String okrug;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("home")
    @Expose
    private String home;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("children")
    @Expose
    private String children;
    @SerializedName("interests")
    @Expose
    private String interests;
    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("car")
    @Expose
    private String car;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("token")
    @Expose
    private String token;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserphoto() {
        return userphoto;
    }

    public void setUserphoto(String userphoto) {
        this.userphoto = userphoto;
    }

    public String getThrumbnail() {
        return thrumbnail;
    }

    public void setThrumbnail(String thrumbnail) {
        this.thrumbnail = thrumbnail;
    }

    public String getSmscode() {
        return smscode;
    }

    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getRecoveryKey() {
        return recoveryKey;
    }

    public void setRecoveryKey(String recoveryKey) {
        this.recoveryKey = recoveryKey;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getTeamPoints() {
        return teamPoints;
    }

    public void setTeamPoints(String teamPoints) {
        this.teamPoints = teamPoints;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOkrug() {
        return okrug;
    }

    public void setOkrug(String okrug) {
        this.okrug = okrug;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.username);
        dest.writeString(this.name);
        dest.writeString(this.authKey);
        dest.writeString(this.passwordHash);
        dest.writeString(this.passwordResetToken);
        dest.writeString(this.role);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.status);
        dest.writeString(this.type);
        dest.writeString(this.level);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.userphoto);
        dest.writeString(this.thrumbnail);
        dest.writeString(this.smscode);
        dest.writeString(this.pincode);
        dest.writeString(this.timestamp);
        dest.writeString(this.timeZone);
        dest.writeString(this.recoveryKey);
        dest.writeString(this.points);
        dest.writeString(this.teamPoints);
        dest.writeString(this.deposit);
        dest.writeString(this.categories);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.sponsor);
        dest.writeString(this.city);
        dest.writeString(this.okrug);
        dest.writeString(this.street);
        dest.writeString(this.home);
        dest.writeString(this.sex);
        dest.writeString(this.birthdate);
        dest.writeString(this.children);
        dest.writeString(this.interests);
        dest.writeString(this.family);
        dest.writeString(this.car);
        dest.writeString(this.education);
        dest.writeString(this.password);
        dest.writeString(this.token);
    }

    public ReferalModel() {
    }

    protected ReferalModel(Parcel in) {
        this.id = in.readString();
        this.username = in.readString();
        this.name = in.readString();
        this.authKey = in.readString();
        this.passwordHash = in.readString();
        this.passwordResetToken = in.readString();
        this.role = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.status = in.readString();
        this.type = in.readString();
        this.level = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.userphoto = in.readString();
        this.thrumbnail = in.readString();
        this.smscode = in.readString();
        this.pincode = in.readString();
        this.timestamp = in.readString();
        this.timeZone = in.readString();
        this.recoveryKey = in.readString();
        this.points = in.readString();
        this.teamPoints = in.readString();
        this.deposit = in.readString();
        this.categories = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.sponsor = in.readString();
        this.city = in.readString();
        this.okrug = in.readString();
        this.street = in.readString();
        this.home = in.readString();
        this.sex = in.readString();
        this.birthdate = in.readString();
        this.children = in.readString();
        this.interests = in.readString();
        this.family = in.readString();
        this.car = in.readString();
        this.education = in.readString();
        this.password = in.readString();
        this.token = in.readString();
    }

    public static final Parcelable.Creator<ReferalModel> CREATOR = new Parcelable.Creator<ReferalModel>() {
        @Override
        public ReferalModel createFromParcel(Parcel source) {
            return new ReferalModel(source);
        }

        @Override
        public ReferalModel[] newArray(int size) {
            return new ReferalModel[size];
        }
    };
}
