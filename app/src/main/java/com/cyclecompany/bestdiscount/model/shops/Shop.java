
package com.cyclecompany.bestdiscount.model.shops;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("auth_key")
    @Expose
    private String authKey;
    @SerializedName("password_hash")
    @Expose
    private String passwordHash;
    @SerializedName("password_reset_token")
    @Expose
    private String passwordResetToken;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("userphoto")
    @Expose
    private String userphoto;
    @SerializedName("thrumbnail")
    @Expose
    private String thrumbnail;
    @SerializedName("smscode")
    @Expose
    private String smscode;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("time_zone")
    @Expose
    private String timeZone;
    @SerializedName("recovery_key")
    @Expose
    private String recoveryKey;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("team_points")
    @Expose
    private String teamPoints;
    @SerializedName("deposit")
    @Expose
    private String deposit;
    @SerializedName("categories")
    @Expose
    private String categories;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("okrug")
    @Expose
    private String okrug;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("home")
    @Expose
    private String home;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("children")
    @Expose
    private String children;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *     The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The authKey
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * 
     * @param authKey
     *     The auth_key
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    /**
     * 
     * @return
     *     The passwordHash
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * 
     * @param passwordHash
     *     The password_hash
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * 
     * @return
     *     The passwordResetToken
     */
    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    /**
     * 
     * @param passwordResetToken
     *     The password_reset_token
     */
    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    /**
     * 
     * @return
     *     The role
     */
    public String getRole() {
        return role;
    }

    /**
     * 
     * @param role
     *     The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The level
     */
    public String getLevel() {
        return level;
    }

    /**
     * 
     * @param level
     *     The level
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return
     *     The userphoto
     */
    public String getUserphoto() {
        return userphoto;
    }

    /**
     * 
     * @param userphoto
     *     The userphoto
     */
    public void setUserphoto(String userphoto) {
        this.userphoto = userphoto;
    }

    /**
     * 
     * @return
     *     The thrumbnail
     */
    public String getThrumbnail() {
        return thrumbnail;
    }

    /**
     * 
     * @param thrumbnail
     *     The thrumbnail
     */
    public void setThrumbnail(String thrumbnail) {
        this.thrumbnail = thrumbnail;
    }

    /**
     * 
     * @return
     *     The smscode
     */
    public String getSmscode() {
        return smscode;
    }

    /**
     * 
     * @param smscode
     *     The smscode
     */
    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }

    /**
     * 
     * @return
     *     The pincode
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * 
     * @param pincode
     *     The pincode
     */
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * 
     * @return
     *     The token
     */
    public String getToken() {
        return token;
    }

    /**
     * 
     * @param token
     *     The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 
     * @return
     *     The timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * 
     * @param timeZone
     *     The time_zone
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * 
     * @return
     *     The recoveryKey
     */
    public String getRecoveryKey() {
        return recoveryKey;
    }

    /**
     * 
     * @param recoveryKey
     *     The recovery_key
     */
    public void setRecoveryKey(String recoveryKey) {
        this.recoveryKey = recoveryKey;
    }

    /**
     * 
     * @return
     *     The points
     */
    public String getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The teamPoints
     */
    public String getTeamPoints() {
        return teamPoints;
    }

    /**
     * 
     * @param teamPoints
     *     The team_points
     */
    public void setTeamPoints(String teamPoints) {
        this.teamPoints = teamPoints;
    }

    /**
     * 
     * @return
     *     The deposit
     */
    public String getDeposit() {
        return deposit;
    }

    /**
     * 
     * @param deposit
     *     The deposit
     */
    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    /**
     * 
     * @return
     *     The categories
     */
    public String getCategories() {
        return categories;
    }

    /**
     * 
     * @param categories
     *     The categories
     */
    public void setCategories(String categories) {
        this.categories = categories;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 
     * @return
     *     The city
     */
    public String getCity() {
        return city;
    }

    /**
     * 
     * @param city
     *     The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 
     * @return
     *     The okrug
     */
    public String getOkrug() {
        return okrug;
    }

    /**
     * 
     * @param okrug
     *     The okrug
     */
    public void setOkrug(String okrug) {
        this.okrug = okrug;
    }

    /**
     * 
     * @return
     *     The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * 
     * @param street
     *     The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * 
     * @return
     *     The home
     */
    public String getHome() {
        return home;
    }

    /**
     * 
     * @param home
     *     The home
     */
    public void setHome(String home) {
        this.home = home;
    }

    /**
     * 
     * @return
     *     The sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * 
     * @param sex
     *     The sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 
     * @return
     *     The birthdate
     */
    public String getBirthdate() {
        return birthdate;
    }

    /**
     * 
     * @param birthdate
     *     The birthdate
     */
    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * 
     * @return
     *     The children
     */
    public String getChildren() {
        return children;
    }

    /**
     * 
     * @param children
     *     The children
     */
    public void setChildren(String children) {
        this.children = children;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.username);
        dest.writeString(this.name);
        dest.writeString(this.authKey);
        dest.writeString(this.passwordHash);
        dest.writeString(this.passwordResetToken);
        dest.writeString(this.role);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.status);
        dest.writeString(this.type);
        dest.writeString(this.level);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.password);
        dest.writeString(this.userphoto);
        dest.writeString(this.thrumbnail);
        dest.writeString(this.smscode);
        dest.writeString(this.pincode);
        dest.writeString(this.token);
        dest.writeString(this.timestamp);
        dest.writeString(this.timeZone);
        dest.writeString(this.recoveryKey);
        dest.writeString(this.points);
        dest.writeString(this.teamPoints);
        dest.writeString(this.deposit);
        dest.writeString(this.categories);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.city);
        dest.writeString(this.okrug);
        dest.writeString(this.street);
        dest.writeString(this.home);
        dest.writeString(this.sex);
        dest.writeString(this.birthdate);
        dest.writeString(this.children);
    }

    public Shop() {
    }

    protected Shop(Parcel in) {
        this.id = in.readString();
        this.username = in.readString();
        this.name = in.readString();
        this.authKey = in.readString();
        this.passwordHash = in.readString();
        this.passwordResetToken = in.readString();
        this.role = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.status = in.readString();
        this.type = in.readString();
        this.level = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.password = in.readString();
        this.userphoto = in.readString();
        this.thrumbnail = in.readString();
        this.smscode = in.readString();
        this.pincode = in.readString();
        this.token = in.readString();
        this.timestamp = in.readString();
        this.timeZone = in.readString();
        this.recoveryKey = in.readString();
        this.points = in.readString();
        this.teamPoints = in.readString();
        this.deposit = in.readString();
        this.categories = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.city = in.readString();
        this.okrug = in.readString();
        this.street = in.readString();
        this.home = in.readString();
        this.sex = in.readString();
        this.birthdate = in.readString();
        this.children = in.readString();
    }

    public static final Parcelable.Creator<Shop> CREATOR = new Parcelable.Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel source) {
            return new Shop(source);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };
}
