
package com.cyclecompany.bestdiscount.model.shop_history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopHistoryData {

    @SerializedName("data")
    @Expose
    private List<ShopHistory> data = null;
    @SerializedName("message")
    @Expose
    private Message message;

    public List<ShopHistory> getData() {
        return data;
    }

    public void setData(List<ShopHistory> data) {
        this.data = data;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

}
