package com.cyclecompany.bestdiscount.model.realm;


import com.cyclecompany.bestdiscount.model.register.UserModel;

import io.realm.RealmObject;

/**
 * Created by Илья on 24.10.2016.
 */

public class SettingsObject extends RealmObject {

    private UserModelRealm userModelRealm;



    public SettingsObject() {
    }



    public SettingsObject(UserModel userModel) {
        this.userModelRealm = new UserModelRealm(
                userModel.getId(),
                userModel.getEmail(),
                userModel.getStatus(),
                userModel.getType(),
                userModel.getPoints(),
                userModel.getUsername(),
                userModel.getPhone(),
                userModel.getSmscode(),
                userModel.getToken()


        );
    }

    public String getToken() {
        return userModelRealm.getToken();
    }

    public UserModelRealm getUserModelRealm() {
        return userModelRealm;
    }

    public void setUserModelRealm(UserModelRealm userModelRealm) {
        this.userModelRealm = userModelRealm;
    }
}
