
package com.cyclecompany.bestdiscount.model.userSignUpInit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Data {

    @SerializedName("username")
    
    private String username;
    @SerializedName("email")
    
    private String email;
    @SerializedName("phone")
    
    private String phone;
    @SerializedName("smscode")
    
    private String smscode;
    @SerializedName("password")
    
    private String password;
    @SerializedName("token")
    
    private String token;
    @SerializedName("timestamp")
    
    private String timestamp;
    @SerializedName("id")
    
    private String id;
    @SerializedName("name")
    
    private String name;
    @SerializedName("type")
    
    private String type;
    @SerializedName("time_zone")
    
    private String timeZone;
    @SerializedName("recovery_key")
    
    private String recoveryKey;
    @SerializedName("points")
    
    private String points;
    @SerializedName("team_points")
    
    private String teamPoints;

    /**
     * 
     * @return
     *     The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *     The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The smscode
     */
    public String getSmscode() {
        return smscode;
    }

    /**
     * 
     * @param smscode
     *     The smscode
     */
    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }

    /**
     * 
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return
     *     The token
     */
    public String getToken() {
        return token;
    }

    /**
     * 
     * @param token
     *     The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * 
     * @param timeZone
     *     The time_zone
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * 
     * @return
     *     The recoveryKey
     */
    public String getRecoveryKey() {
        return recoveryKey;
    }

    /**
     * 
     * @param recoveryKey
     *     The recovery_key
     */
    public void setRecoveryKey(String recoveryKey) {
        this.recoveryKey = recoveryKey;
    }

    /**
     * 
     * @return
     *     The points
     */
    public String getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The teamPoints
     */
    public String getTeamPoints() {
        return teamPoints;
    }

    /**
     * 
     * @param teamPoints
     *     The team_points
     */
    public void setTeamPoints(String teamPoints) {
        this.teamPoints = teamPoints;
    }

}
