package com.cyclecompany.bestdiscount.model.pincode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ilyasavin on 12/2/16.
 */

public class Pincode {

    @SerializedName("pincode")
    @Expose
    private Integer pincode;

    /**
     *
     * @return
     * The pincode
     */
    public Integer getPincode() {
        return pincode;
    }

    /**
     *
     * @param pincode
     * The pincode
     */
    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }
    }

