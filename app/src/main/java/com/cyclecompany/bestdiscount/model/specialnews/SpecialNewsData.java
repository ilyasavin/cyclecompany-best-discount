
package com.cyclecompany.bestdiscount.model.specialnews;

import com.cyclecompany.bestdiscount.model.login.Message;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class SpecialNewsData {

    @SerializedName("data")
    @Expose
    private List<SpecialNewsModel> data = new ArrayList<SpecialNewsModel>();
    @SerializedName("message")
    @Expose
    private Message message;

    /**
     * 
     * @return
     *     The data
     */
    public List<SpecialNewsModel> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<SpecialNewsModel> data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(Message message) {
        this.message = message;
    }

}
