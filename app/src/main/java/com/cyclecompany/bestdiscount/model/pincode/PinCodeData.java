package com.cyclecompany.bestdiscount.model.pincode;

import com.cyclecompany.bestdiscount.model.login.Message;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class PinCodeData {

@SerializedName("data")
@Expose
private List<Pincode> data = new ArrayList<Pincode>();
@SerializedName("message")
@Expose
private Message message;

/**
* 
* @return
* The data
*/
public List<Pincode> getData() {
return data;
}

/**
* 
* @param data
* The data
*/
public void setData(List<Pincode> data) {
this.data = data;
}

/**
* 
* @return
* The message
*/
public Message getMessage() {
return message;
}

/**
* 
* @param message
* The message
*/
public void setMessage(Message message) {
this.message = message;
}

}
