package com.cyclecompany.bestdiscount.model.register;

/**
 * Created by Илья on 21.10.2016.
 */
public class RequestSignUpModel {

    String username;
    String email;
    String password;
    String confirm_password;

    public RequestSignUpModel(String username, String email, String password, String confirm_password) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirm_password = confirm_password;
    }
}
