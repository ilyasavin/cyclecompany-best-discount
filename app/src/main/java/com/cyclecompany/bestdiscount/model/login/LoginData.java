
package com.cyclecompany.bestdiscount.model.login;

import com.cyclecompany.bestdiscount.model.register.UserModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class LoginData {

    @SerializedName("data")
    @Expose
    private List<UserModel> data = new ArrayList<UserModel>();
    @SerializedName("message")
    @Expose
    private Message message;

    /**
     * 
     * @return
     *     The data
     */
    public List<UserModel> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<UserModel> data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(Message message) {
        this.message = message;
    }

}
