package com.cyclecompany.bestdiscount.model.categories;

import com.cyclecompany.bestdiscount.model.login.Message;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoriesData {

@SerializedName("data")
@Expose
private List<List<CategoryItem>> data = null;
@SerializedName("message")
@Expose
private Message message;


public List<List<CategoryItem>> getData() {
return data;
}

public void setData(List<List<CategoryItem>> data) {
this.data = data;
}

public Message getMessage() {
return message;
}

public void setMessage(Message message) {
this.message = message;
}

}