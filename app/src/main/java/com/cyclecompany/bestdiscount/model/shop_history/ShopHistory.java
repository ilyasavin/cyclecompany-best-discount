
package com.cyclecompany.bestdiscount.model.shop_history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShopHistory {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_shop")
    @Expose
    private String idShop;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("purchase_name")
    @Expose
    private String purchaseName;
    @SerializedName("ocenka")
    @Expose
    private String ocenka;
    @SerializedName("otziv")
    @Expose
    private String otziv;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("shop_name")
    @Expose
    private String shopName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdShop() {
        return idShop;
    }

    public void setIdShop(String idShop) {
        this.idShop = idShop;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPurchaseName() {
        return purchaseName;
    }

    public void setPurchaseName(String purchaseName) {
        this.purchaseName = purchaseName;
    }

    public String getOcenka() {
        return ocenka;
    }

    public void setOcenka(String ocenka) {
        this.ocenka = ocenka;
    }

    public String getOtziv() {
        return otziv;
    }

    public void setOtziv(String otziv) {
        this.otziv = otziv;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

}
