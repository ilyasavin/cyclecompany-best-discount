package com.cyclecompany.bestdiscount.model;

/**
 * Created by Илья on 13.10.2016.
 */

public class SignUpUserInfo
{

    private String mUserName;
    private String mEmail;
    private String mPassword;
    private String mConfirmPassword;

    public SignUpUserInfo(String mUserName, String mEmail, String mPassword, String mConfirmPassword) {
        this.mUserName = mUserName;
        this.mEmail = mEmail;
        this.mPassword = mPassword;
        this.mConfirmPassword = mConfirmPassword;
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmConfirmPassword() {
        return mConfirmPassword;
    }

    public void setmConfirmPassword(String mConfirmPassword) {
        this.mConfirmPassword = mConfirmPassword;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }
}
