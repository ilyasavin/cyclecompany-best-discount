package com.cyclecompany.bestdiscount.model.photo;

import com.cyclecompany.bestdiscount.model.login.Message;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhotoData {

@SerializedName("data")
@Expose
private List<Object> data = null;
@SerializedName("message")
@Expose
private Message message;

public List<Object> getData() {
return data;
}

public void setData(List<Object> data) {
this.data = data;
}

public Message getMessage() {
return message;
}

public void setMessage(Message message) {
this.message = message;
}

}