package com.cyclecompany.bestdiscount.model.login;

/**
 * Created by Илья on 21.10.2016.
 */
public class RequestSignInModel {

    private String email;
    private String password;
    private String token;
    private boolean temp = true;

    public RequestSignInModel(String password, String email) {
        this.password = password;
        this.email = email;
    }

    public RequestSignInModel(String email, String token, boolean temp) {
        this.email = email;
        this.token = token;
    }
}
