
package com.cyclecompany.bestdiscount.model.shops;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AllShopsData {

    @SerializedName("data")
    @Expose
    private List<Shop> data = new ArrayList<Shop>();
    @SerializedName("message")
    @Expose
    private Message message;

    /**
     * 
     * @return
     *     The data
     */
    public List<Shop> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<Shop> data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(Message message) {
        this.message = message;
    }

}
