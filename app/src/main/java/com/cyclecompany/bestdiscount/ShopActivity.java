package com.cyclecompany.bestdiscount;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.cyclecompany.bestdiscount.adapters.NewsShopRVAdapter;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.shops.Shop;
import com.cyclecompany.bestdiscount.model.specialnews.SpecialNewsData;
import com.cyclecompany.bestdiscount.model.specialnews.SpecialNewsModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

import butterknife.Bind;

public class ShopActivity extends BaseActivity {

    @Bind(R.id.postImage)
    ImageView mPostImage;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.rv)
    RecyclerView mRecyclerView;

    private Shop mShop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        mShop = getIntent().getParcelableExtra("shop");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            mPostImage.setTransitionName("ReferalTrans");
        }

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(layoutManager);


        mToolbar.setNavigationIcon(R.drawable.ic_arrow_left_black_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.showOverflowMenu();

        mToolbar.setTitle(mShop.getName());

        Picasso.with(this).load(mShop.getUserphoto()).into(mPostImage);

        getSpecialNewsById();


    }

    private void getSpecialNewsById() {

        DataService.init().getSpecialNews(new DataService.onRequestSpecialNewsResult() {
            @Override
            public void onRequestSpecialNewsResult(SpecialNewsData specialNewsData, String cookie) {

                NewsShopRVAdapter newsRVAdapter =
                        new NewsShopRVAdapter(ShopActivity.this, sortNews(specialNewsData));

                mRecyclerView.setAdapter(newsRVAdapter);

            }
        }, realm);


    }

    private List<SpecialNewsModel> sortNews(SpecialNewsData specialNewsData){
        for (int i = 0; i <  specialNewsData.getData().size(); i++) {

            if(!Objects.equals(specialNewsData.getData().get(i).getShopId(), mShop.getId()))
                specialNewsData.getData().remove(i);
        }

        return specialNewsData.getData();
    }
}
