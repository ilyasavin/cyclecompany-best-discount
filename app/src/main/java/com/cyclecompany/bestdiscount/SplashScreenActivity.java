package com.cyclecompany.bestdiscount;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;

import com.cyclecompany.bestdiscount.components.Constants;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;


public class SplashScreenActivity extends BaseActivity {

    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        if (settings.getInt("isAuthorized", 0) == 0)
                 {

                Intent i = new Intent(this,FirstStartActivity.class);
                startActivity(i);

        }

                else {

            setContentView(R.layout.activity_splash_screen);
            initViewElements();
            setLoading();

        }





    }

    @Override
    protected void onResume() {
        super.onResume();
        //setLoading();
    }

    private void setLoading() {

        countDownTimer = new CountDownTimer(Constants.LOADING_TIME, Constants.LOADING_INTERVAL) {
            int index=0;

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                countDownTimer.cancel();
                SplashScreenActivity.this.finish();
                ActivityNavigator.startMainActivity(getApplicationContext());

            }

        }.start();
    }

    void initViewElements(){

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(countDownTimer!=null)
            countDownTimer.cancel();

    }
}
