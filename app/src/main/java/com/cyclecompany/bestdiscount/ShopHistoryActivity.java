package com.cyclecompany.bestdiscount;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.cyclecompany.bestdiscount.adapters.ShopHistoryRVAdapter;
import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.model.shop_history.ShopHistoryData;
import com.cyclecompany.bestdiscount.util.RealmUtils;

import butterknife.Bind;

public class ShopHistoryActivity extends BaseActivity {

    @Bind(R.id.shopRV)
    RecyclerView mRecyclerView;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_history);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_left_black_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.showOverflowMenu();

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(layoutManager);

        DataService.init().getShopHistory(this, new DataService.onRequestHistoryResult() {
            @Override
            public void onRequestHistoryResylt(ShopHistoryData shopHistoryData) {

                ShopHistoryRVAdapter adapter = new ShopHistoryRVAdapter(ShopHistoryActivity.this, shopHistoryData.getData());

                mRecyclerView.setAdapter(adapter);
            }
        }, RealmUtils.getUserSettings(realm).getUserModelRealm().getId(), RealmUtils.getUserSettings(realm).getToken());

    }

}
