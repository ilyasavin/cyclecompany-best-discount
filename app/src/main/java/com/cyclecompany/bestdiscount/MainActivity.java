package com.cyclecompany.bestdiscount;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.cyclecompany.bestdiscount.data.DataService;
import com.cyclecompany.bestdiscount.fragments.NewsFragment;
import com.cyclecompany.bestdiscount.fragments.ReferalsFragment;
import com.cyclecompany.bestdiscount.fragments.ShopFragment;
import com.cyclecompany.bestdiscount.fragments.UserFragment;
import com.cyclecompany.bestdiscount.model.photo.PhotoData;
import com.cyclecompany.bestdiscount.navigator.ActivityNavigator;
import com.cyclecompany.bestdiscount.navigator.FragmentNavigator;
import com.cyclecompany.bestdiscount.util.RealmUtils;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.File;

import butterknife.Bind;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.bottomBar)
    BottomBar mBottomBar;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.mainFrame)
    FrameLayout mFragmentFrame;
    @Bind(R.id.search_view)
    MaterialSearchView mSearchView;
    @Bind(R.id.appbar)
    AppBarLayout appBarLayout;
    public static TextView mTitleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewElements(savedInstanceState, mToolbar);

    }

    private void initViewElements(Bundle savedInstanceState, final Toolbar toolbar) {

        //initDrawer(toolbar, this);

        mToolbar.setTitle("The best discount");
        setSupportActionBar(mToolbar);

        mTitleText = (TextView) findViewById(R.id.toolbar_title);

        mSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        mSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
            }

            @Override
            public void onSearchViewClosed() {
            }
        });


        for (int i = 0; i < mBottomBar.getTabCount(); i++) {
        }
        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case (R.id.tab_referals):
                        mTitleText.setText("Мои рефералы");
                        FragmentNavigator.showContentFragment(MainActivity.this, ReferalsFragment.newInstance());
                        FragmentNavigator.removeContentFragment(MainActivity.this);
                        break;
                    case (R.id.tab_news):
                        mTitleText.setText("Новости");
                        FragmentNavigator.removeContentFragment(MainActivity.this);
                        FragmentNavigator.showContentFragment(MainActivity.this, NewsFragment.newInstance());
                        break;
                    case (R.id.tab_user):
                        mTitleText.setText("Профиль");
                        FragmentNavigator.removeContentFragment(MainActivity.this);
                        FragmentNavigator.showContentFragment(MainActivity.this, UserFragment.newInstance());
                        break;
                    case (R.id.tab_places):
                        mTitleText.setText("Категории");
                        FragmentNavigator.removeContentFragment(MainActivity.this);
                        FragmentNavigator.showContentFragment(MainActivity.this, ShopFragment.newInstance());
                        break;
                }
            }
        });

    }


    @Override
    public void onRefresh() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            clearApplicationData();
            finish();
            ActivityNavigator.startFirstStartActivity(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    //Some error handling
                    int a = 5;
                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                    String filePath = imageFile.getPath();
                    Bitmap bitmap = BitmapFactory.decodeFile(filePath);

                    UserFragment.userPhoto.setImageBitmap(bitmap);

                    DataService.init().sendPhoto(imageFile, new DataService.onRequestPhotoResult() {
                        @Override
                        public void onRequestPhotoResult(PhotoData photoData) {
                            int a = 5;
                        }
                    }, RealmUtils.getUserSettings(realm).getToken());
                }

                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {
                    //Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source == EasyImage.ImageSource.CAMERA) {
                        File photoFile = EasyImage.lastlyTakenButCanceledPhoto(MainActivity.this);
                        if (photoFile != null) photoFile.delete();
                    }
                }
            });

        }



    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // You have to save path in case your activity is killed.
        // In such a scenario, you will need to re-initialize the CameraImagePicker
        outState.putString("picker_path", UserFragment.url);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // After Activity recreate, you need to re-initialize these
        // two values to be able to re-initialize CameraImagePicker
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                UserFragment.url = savedInstanceState.getString("picker_path");
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }


}



