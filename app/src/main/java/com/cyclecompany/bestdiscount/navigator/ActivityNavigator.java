package com.cyclecompany.bestdiscount.navigator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;

import com.cyclecompany.bestdiscount.AnketaActivity;
import com.cyclecompany.bestdiscount.FirstStartActivity;
import com.cyclecompany.bestdiscount.InviteFriendActivity;
import com.cyclecompany.bestdiscount.MainActivity;
import com.cyclecompany.bestdiscount.ReferalActivity;
import com.cyclecompany.bestdiscount.RegistrationActivity;
import com.cyclecompany.bestdiscount.ScannerActivity;
import com.cyclecompany.bestdiscount.ShopActivity;
import com.cyclecompany.bestdiscount.ShopHistoryActivity;
import com.cyclecompany.bestdiscount.SplashScreenActivity;
import com.cyclecompany.bestdiscount.model.shops.Shop;

/**
 * Activity navigation.
 */
public class ActivityNavigator {

    public static void startReferalActivity(Activity context, Parcelable parcelable, View imageView) {

        Intent intent = new Intent(context, ReferalActivity.class);

        intent.putExtra("Referal", parcelable);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            imageView.setTransitionName("ReferalTrans");
            Pair<View, String> pair1 = Pair.create(imageView, imageView.getTransitionName());
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(context, pair1);
            context.startActivity(intent, options.toBundle());
        } else
            context.startActivity(intent);
    }

    public static void startMainActivity(Context context){
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void startAnketa(Context context){
        Intent intent = new Intent(context, AnketaActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void startScannerActivity(Context context){
        Intent intent = new Intent(context,ScannerActivity.class);
        context.startActivity(intent);
    }
    public static void startSplashActivity(Context context){
        Intent intent = new Intent(context, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
    public static void startRegistrationActivity(Context context, String userId){
        Intent intent = new Intent(context,RegistrationActivity.class);
        intent.putExtra("user_id",userId);
        context.startActivity(intent);
    }
    public static void startInviteActivity(Context context){
        Intent intent = new Intent(context,InviteFriendActivity.class);
        context.startActivity(intent);
    }

    public static void startShopHistoryActivity(Context context){
        Intent intent = new Intent(context,ShopHistoryActivity.class);
        context.startActivity(intent);
    }

    public static void startShopActivity(Activity context, Shop shop, View imageView){
        Intent intent = new Intent(context,ShopActivity.class);
        intent.putExtra("shop",shop);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            imageView.setTransitionName("ShopTrans");
            Pair<View, String> pair1 = Pair.create(imageView, imageView.getTransitionName());
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(context, pair1);
            context.startActivity(intent, options.toBundle());
        } else
            context.startActivity(intent);
    }


    public static void startFirstStartActivity(Context context) {
        Intent intent = new Intent(context, FirstStartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
}

