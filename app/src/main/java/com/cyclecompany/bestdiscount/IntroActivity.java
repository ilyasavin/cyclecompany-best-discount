package com.cyclecompany.bestdiscount;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class IntroActivity extends AppIntro2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance("Теперь за то, что вы покупаете в любимых\n" +
                "магазинах - Вам платят деньги.","Накапливай баллы за покупки и трать в любом магазине-партнере компании. ", R.drawable.rubl, Color.parseColor("#212121")));
        addSlide(AppIntroFragment.newInstance("Уникальная реферальная система!","Рассказывая друзьям можно получать баллы от их покупок.\n", R.drawable.priglash, Color.parseColor("#212121")));
        addSlide(AppIntroFragment.newInstance("Оплачивай сотовую связь баллами. ","", R.drawable.slide_phone_1, Color.parseColor("#212121")));
        addSlide(AppIntroFragment.newInstance("Удобный поиск нужного магазина","", R.drawable.like, Color.parseColor("#212121")));
        addSlide(AppIntroFragment.newInstance("Переходи на новый статус и получай подарки от компании!", "",R.drawable.vozmoj, Color.parseColor("#212121")));
        addSlide(AppIntroFragment.newInstance("Обменивай баллы на деньги!", "",R.drawable.rubl, Color.parseColor("#212121")));

        showSkipButton(false);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}